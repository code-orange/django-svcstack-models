from datetime import timedelta, datetime, time

from django.db import models
from django.utils import timezone

from dirtyfields import DirtyFieldsMixin

from django_mdat_customer.django_mdat_customer.models import MdatCustomers, MdatItems


class SvcstackInstance(models.Model):
    name = models.CharField(max_length=30)
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "svcstack_instance"


class SvcstackContact(models.Model):
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    class Meta:
        db_table = "svcstack_contact"


class SvcstackBillCustomerToCustomer(models.Model):
    customer = models.OneToOneField(MdatCustomers, models.DO_NOTHING, primary_key=True)
    bill_to_customer = models.ForeignKey(
        MdatCustomers, models.DO_NOTHING, related_name="+"
    )

    class Meta:
        db_table = "svcstack_bill_customer_to_customer"


class SvcstackActivity(DirtyFieldsMixin, models.Model):
    instance = models.ForeignKey(SvcstackInstance, models.DO_NOTHING)
    subject = models.CharField(max_length=200)
    customer = models.ForeignKey(MdatCustomers, on_delete=models.DO_NOTHING)
    contact = models.ForeignKey(SvcstackContact, on_delete=models.DO_NOTHING)
    on_site = models.BooleanField(default=True)
    date_start = models.DateTimeField(default=timezone.now)
    date_end = models.DateTimeField(null=True, blank=True)
    billing_duration = models.DurationField(null=True, blank=True, editable=False)

    def save(self, *args, **kwargs):
        start = self.date_start
        end = self.date_end

        if isinstance(self.date_start, str):
            start = datetime.strptime(self.date_start, "%Y-%m-%d %H:%M:%S.%fZ")

        if isinstance(self.date_end, str):
            end = datetime.strptime(self.date_end, "%Y-%m-%d %H:%M:%S.%fZ")

        if end:
            duration = end - start
            duration_in_minutes = duration.seconds / 60
            remainder = duration_in_minutes % 15
            self.billing_duration = duration + timedelta(
                minutes=(15 if remainder >= 5 else 0) - remainder
            )

        super().save(*args, **kwargs)

    def __str__(self):
        return self.subject

    class Meta:
        ordering = ["-date_start"]
        db_table = "svcstack_activity"


class SvcstackContract(models.Model):
    customer = models.OneToOneField(MdatCustomers, models.DO_NOTHING, primary_key=True)

    travel_allowance = models.ForeignKey(MdatItems, models.DO_NOTHING)

    hours_included = models.FloatField(default=0)

    factor_business_hours = models.FloatField(default=1.0)
    factor_outside_business_hours = models.FloatField(default=1.5)
    factor_holidays = models.FloatField(default=2.0)

    business_hours_monday_start = models.TimeField(default=time(hour=12, minute=0))
    business_hours_monday_end = models.TimeField(default=time(hour=17, minute=0))

    business_hours_tuesday_start = models.TimeField(default=time(hour=12, minute=0))
    business_hours_tuesday_end = models.TimeField(default=time(hour=17, minute=0))

    business_hours_wednesday_start = models.TimeField(default=time(hour=12, minute=0))
    business_hours_wednesday_end = models.TimeField(default=time(hour=17, minute=0))

    business_hours_thursday_start = models.TimeField(default=time(hour=12, minute=0))
    business_hours_thursday_end = models.TimeField(default=time(hour=17, minute=0))

    business_hours_friday_start = models.TimeField(default=time(hour=12, minute=0))
    business_hours_friday_end = models.TimeField(default=time(hour=17, minute=0))

    business_hours_saturday_start = models.TimeField(default=time(hour=0, minute=0))
    business_hours_saturday_end = models.TimeField(default=time(hour=0, minute=0))

    business_hours_sunday_start = models.TimeField(default=time(hour=0, minute=0))
    business_hours_sunday_end = models.TimeField(default=time(hour=0, minute=0))

    class Meta:
        db_table = "svcstack_contract"
