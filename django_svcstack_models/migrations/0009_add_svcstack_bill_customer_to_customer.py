# Generated by Django 3.2.14 on 2022-07-21 21:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("django_mdat_customer", "0043_add_mdat_item_bill_of_materials"),
        ("django_svcstack_models", "0008_add_contact_to_activity"),
    ]

    operations = [
        migrations.CreateModel(
            name="SvcstackBillCustomerToCustomer",
            fields=[
                (
                    "customer",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        primary_key=True,
                        serialize=False,
                        to="django_mdat_customer.mdatcustomers",
                    ),
                ),
                (
                    "bill_to_customer",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        related_name="+",
                        to="django_mdat_customer.mdatcustomers",
                    ),
                ),
            ],
            options={
                "db_table": "svcstack_bill_customer_to_customer",
            },
        ),
    ]
